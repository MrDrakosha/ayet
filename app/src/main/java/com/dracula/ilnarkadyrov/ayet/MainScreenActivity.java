package com.dracula.ilnarkadyrov.ayet;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.res.AssetManager;
import android.databinding.DataBindingUtil;
import android.graphics.Typeface;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.SparseIntArray;
import android.view.MenuItem;
import android.view.Window;
import android.view.WindowManager;
import android.widget.TextView;
import com.dracula.ilnarkadyrov.ayet.databinding.ActivityMainScreenBinding;

import java.util.Calendar;
import java.util.List;

public class MainScreenActivity extends AppCompatActivity {

    public static ActivityMainScreenBinding bind;
    //private VpAdapter adapter;

    private TextView toolbarTitle;

    // collections
    private SparseIntArray items;

    public static final String DATA = "com.dracula.ilnarkadyrov.ayet.data";
    public static final String TODAY = "today";
    public static final String FIRST = "first";
    public static final String TODAYDAY = "todayday";

    public static int day;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        bind = DataBindingUtil.setContentView(this, R.layout.activity_main_screen);

        Calendar calendar = Calendar.getInstance();
        int curDay = calendar.get(Calendar.DATE);

        SharedPreferences preferences = getSharedPreferences(DATA, Context.MODE_PRIVATE);
        int data = preferences.getInt(TODAY, curDay);
        day = preferences.getInt(TODAYDAY, 0);
        boolean first = preferences.getBoolean(FIRST, true);

        SharedPreferences.Editor editor = preferences.edit();
        editor.putInt(TODAY,curDay);
        editor.putInt(TODAYDAY, day);
        editor.commit();


        String[] numbers = getResources().getStringArray(R.array.ayeNumbers);


        if (data == curDay){
            //None
        } else {
            if (day < numbers.length - 1){
                day++;
            } else {
                day = 0;
            }
            editor.putInt(TODAY,curDay);
            editor.putInt(TODAYDAY, day);
            editor.commit();
        }

        Toolbar myToolbar = (Toolbar) findViewById(R.id.my_toolbar);
        setSupportActionBar(myToolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);

        AssetManager am = getApplicationContext().getAssets();
        toolbarTitle = (TextView) findViewById(R.id.toolbar_title);
        Typeface rus = Typeface.createFromAsset(am , "font/rus.ttf");
        toolbarTitle.setTypeface(rus);
        toolbarTitle.setTextSize(20);

        initView();
        initData();
        initEvent();

        //Notifications
        if (first) {
            NotificationHelper.scheduleRepeatingRTCNotification(getApplicationContext());
            NotificationHelper.enableBootReceiver(getApplicationContext());
            editor.putBoolean(FIRST, false);
            editor.commit();
        }
    }

    private void initView() {
        bind.bnve.enableAnimation(false);
        bind.bnve.enableShiftingMode(false);
        bind.bnve.enableItemShiftingMode(false);
        bind.bnve.setIconSize(24, 24);
        bind.bnve.setTextSize(10);
    }

    private void initData() {
        items = new SparseIntArray(4);

        items.put(R.id.i_home, 0);
        items.put(R.id.i_story, 1);
        items.put(R.id.i_favorites, 2);
        items.put(R.id.i_more, 3);

        // set adapter
        //adapter = new VpAdapter(getSupportFragmentManager(), fragments);
        //bind.vp.setAdapter(adapter);
        //bind.vp.setOverScrollMode(View.OVER_SCROLL_NEVER);
    }

    private void initEvent() {


        selectFragment(bind.bnve.getMenu().getItem(0));
        // set listener to change the current item of view pager when click bottom nav item
        bind.bnve.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            private int previousPosition = -1;

            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                int position = items.get(item.getItemId());
                if (previousPosition != position) {
                    previousPosition = position;
                    selectFragment(item);
                    //bind.vp.setCurrentItem(position);
                }
                return true;
            }
        });

    }

    protected void selectFragment(MenuItem item) {
        item.setChecked(true);
        switch (item.getItemId()) {
            case R.id.i_home:
                getSupportFragmentManager().beginTransaction().replace(R.id.frame_layout, new FragmentAyet()).commit();
                break;
            case R.id.i_story:
                getSupportFragmentManager().beginTransaction().replace(R.id.frame_layout, new FragmentStory()).commit();
                break;
            case R.id.i_favorites:
                getSupportFragmentManager().beginTransaction().replace(R.id.frame_layout, new FragmentFavorites()).commit();
                break;
            case R.id.i_more:
                getSupportFragmentManager().beginTransaction().replace(R.id.frame_layout, new FragmentSettings()).commit();
                break;
        }
    }

    /**
     * view pager adapter
     */
    private static class VpAdapter extends FragmentPagerAdapter {
        private List<Fragment> data;

        public VpAdapter(FragmentManager fm, List<Fragment> data) {
            super(fm);
            this.data = data;
        }

        @Override
        public int getCount() {
            return data.size();
        }

        @Override
        public Fragment getItem(int position) {
            return data.get(position);
        }
    }

}
