package com.dracula.ilnarkadyrov.ayet;

import android.content.Intent;
import android.content.res.AssetManager;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;

/**
 * Created by ilnarkadyrov on 02/05/2017.
 */

public class FragmentSettings extends Fragment {

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_settings, container, false);

        AssetManager am = view.getContext().getApplicationContext().getAssets();
        Typeface lobster = Typeface.createFromAsset(am , "font/rus.ttf");
        Typeface sf = Typeface.createFromAsset(am , "font/sf.ttf");

        TextView o = (TextView) view.findViewById(R.id.o);
        o.setTypeface(lobster);
        o.setTextSize(30);

        TextView about = (TextView) view.findViewById(R.id.about);
        about.setTypeface(sf);
        about.setTextSize(16);

        TextView a = (TextView) view.findViewById(R.id.a);
        a.setTypeface(lobster);
        a.setTextSize(30);

        TextView author = (TextView) view.findViewById(R.id.author);
        author.setTypeface(sf);
        author.setTextSize(16);

        TextView ia = (TextView) view.findViewById(R.id.ia);
        ia.setTypeface(lobster);
        ia.setTextSize(20);

        TextView ik = (TextView) view.findViewById(R.id.ik);
        ik.setTypeface(lobster);
        ik.setTextSize(20);

        RelativeLayout inst = (RelativeLayout) view.findViewById(R.id.inst);
        inst.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openInst();
            }
        });

        RelativeLayout vk = (RelativeLayout) view.findViewById(R.id.vk);
        vk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openVk();
            }
        });

        return view;
    }

    public void openInst(){
        try {
            Intent intent = new Intent(Intent.ACTION_VIEW);
            intent.setData(Uri.parse(ConstClass.scheme + "islamic.app"));
            intent.setPackage(ConstClass.nomPackageInfo);
            startActivity(intent);
        }
        catch (android.content.ActivityNotFoundException anfe)
        {
            startActivity(new Intent(Intent.ACTION_VIEW,
                    Uri.parse(ConstClass.path + "islamic.app")));
        }
    }

    public void openVk(){
        try{
            startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(String.format("vkontakte://profile/%d", 265625043))));
        } catch (android.content.ActivityNotFoundException anfe){
            startActivity(new Intent(Intent.ACTION_VIEW,
                    Uri.parse("https://vk.com/id" + "265625043")));
        }
    }

}
