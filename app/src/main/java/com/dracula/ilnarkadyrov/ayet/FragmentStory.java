package com.dracula.ilnarkadyrov.ayet;

import android.content.res.AssetManager;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

/**
 * Created by ilnarkadyrov on 01/05/2017.
 */

public class FragmentStory extends Fragment {

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view =  inflater.inflate(R.layout.fragment_story, container, false);

        String[] ayeStory = getResources().getStringArray(R.array.ayeStory);

        AssetManager am = view.getContext().getApplicationContext().getAssets();
        Typeface rus = Typeface.createFromAsset(am , "font/sf.ttf");
        TextView story = (TextView) view.findViewById(R.id.story);
        story.setTypeface(rus);
        story.setTextSize(FragmentAyet.textSize);
        story.setText(ayeStory[MainScreenActivity.day]);

        RelativeLayout backButton = (RelativeLayout) view.findViewById(R.id.backButton);
        backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MainScreenActivity.bind.bnve.setCurrentItem(0);
            }
        });

        return view;
    }
}
