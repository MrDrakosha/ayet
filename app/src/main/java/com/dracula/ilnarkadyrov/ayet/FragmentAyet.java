package com.dracula.ilnarkadyrov.ayet;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.res.AssetManager;
import android.graphics.Typeface;
import android.icu.text.SimpleDateFormat;
import android.icu.util.Calendar;
import android.icu.util.TimeZone;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.text.format.DateFormat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import java.util.Date;

/**
 * Created by ilnarkadyrov on 01/05/2017.
 */

public class FragmentAyet extends Fragment {

    public static int arabicTextSize = 40;
    public static int textSize = 16;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view =  inflater.inflate(R.layout.fragment_ayet, container, false);

        // final

        String[] ayeArabics = getResources().getStringArray(R.array.ayeArabic);
        String[] ayeRuss = getResources().getStringArray(R.array.ayeRus);
        String[] ayeNumbers = getResources().getStringArray(R.array.ayeNumbers);

        TextView arabicAye = (TextView) view.findViewById(R.id.arabicAye);
        AssetManager am = view.getContext().getApplicationContext().getAssets();
        Typeface arabic = Typeface.createFromAsset(am , "font/arabic.ttf");
        arabicAye.setTypeface(arabic);
        arabicAye.setTextSize(arabicTextSize);
        arabicAye.setText(ayeArabics[MainScreenActivity.day]);

        TextView ayeTranslit = (TextView) view.findViewById(R.id.ayeTranslit);
        Typeface rus = Typeface.createFromAsset(am , "font/sf.ttf");
        ayeTranslit.setTypeface(rus);
        ayeTranslit.setTextSize(textSize);
        ayeTranslit.setText(ayeRuss[MainScreenActivity.day]);

        TextView ayeNumber = (TextView) view.findViewById(R.id.ayeNumber);
        ayeNumber.setTypeface(rus);
        ayeNumber.setTextSize(14);
        ayeNumber.setText(ayeNumbers[MainScreenActivity.day]);

        return view;
    }
}
