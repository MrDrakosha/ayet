import gspread
from oauth2client.service_account import ServiceAccountCredentials

scope = ['https://spreadsheets.google.com/feeds']
credentials = ServiceAccountCredentials.from_json_keyfile_name('SpreedSheetPy-8d28e72c71c6.json', scope)
gc = gspread.authorize(credentials)

f = open('ayeArray.xml', 'w')

wks = gc.open("AyetsForEveryDay").sheet1
numbers = wks.range('A2:A51')
ayeArabic = wks.range('D2:D51')
ayeRus = wks.range('B2:B51')
ayeStory = wks.range('E2:E51')

def writeDataTo(name, cell_list):
    f.write('<string-array name=\"' + name + '\">\n')
    for el in range(0, 50):
        f.write('<item>' + (cell_list[el].value).encode('utf-8') + '</item>\n')
    f.write('</string-array>\n\n\n')

if __name__ == '__main__':
    f.write('<?xml version=\"1.0\" encoding=\"utf-8\"?><resources>\n\n\n')

    writeDataTo("ayeNumbers", numbers)
    writeDataTo("ayeArabic", ayeArabic)
    writeDataTo("ayeRus", ayeRus)
    writeDataTo("ayeStory", ayeStory)
    f.write('</resources>')